# Work Track

**Progresso:** Finalizado<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2019<br />

### Objetivo
Implementar um sistema de linha de comando para alocação de horas para empresa Raro Labs.

### Observação
IDE:  [Visual Studio 2019](https://visualstudio.microsoft.com/)
Linguagem: [C#](https://dotnet.microsoft.com/)
Banco de dados: [LiteDB](https://www.litedb.org/)
Necessário baixar o [ChromeDriver](https://chromedriver.chromium.org/downloads)  na versão do Chrome instalado na sua máquina.

### Comandos
#### Ajuda
	$ track --help
#### Inicializar o banco, com os projetos e colaboradores
    $ track update-base
#### Definir o colaborador global
	$ track global -c <NOME DO COLABORADOR>
#### Listar projetos/colaboradores
	$ track list (-p|-c) 
#### Iniciar uma nova demanda
	$ track start -p <NOME DO PROJETO> [-c <COLABORADOR>] -m <DESCRIÇÃO DA DEMANDA> -e <TIPO DE ESFORÇO> [-x HORA EXTRA] [-n <NOTA>]+
#### Iniciar uma demanda fora de hora
	$ track task -p <NOME DO PROJETO> [-c <COLABORADOR>] -m <DESCRIÇÃO DA DEMANDA> -e <TIPO DE ESFORÇO> [-x HORA EXTRA] -s <DATA E HORA DE INICIO> [-f <DATA E HORA FIM>] [-n <NOTA>]+
#### Criar uma demanda à fazer
	$ track todo -p <NOME DO PROJETO> [-c <COLABORADOR>] -m <DESCRIÇÃO DA DEMANDA> [-n <NOTA>]+
#### Parar a demanda atual
	$ track stop
#### Enviar registros
	$ track submit
#### Visualizar demanda
	$ track show -t <ID DA DEMANDA>
#### Adicionar notas à demanda
	$ track note -t <ID DA DEMANDA> [-n <NOTA>]+
#### Tempo gasto por projeto
	$ track time [-y <ANO>] [-m <MES>] [-d <DIA>]
#### Editar demanda
	$ track edit -t <ID DA DEMANDA> -i <INTERVADO> [-m <DESCRIÇÃO>] [-c <COLABORADOR>] [-p <NOME DO PROJETO>] [-s <DATA HORA INICIO>] [-f <DATA HORA FIM>] [-q <ESTADO>] [-x HORA EXTRA] [-e <TIPO ESFORÇO>]
    
### Contribuição

Esse projeto está concluído e é livre para contribuição.