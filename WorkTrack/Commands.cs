﻿using Base;
using LiteDB;
using Microsoft.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using Base.Components;
using Base.Components.Extensions;
using Base.Components.Utils;
using Base.Models;
using static Base.Models.Enumerations;

namespace WorkTrack {
    class Commands {
        static void Main( string[ ] args ) {
            Startup.Init( );

            var app = new CommandLineApplication {
                Name = "Work Track",
                Description = "Track and submit the time spent on each demand"
            };

            app.HelpOption( "-?|-h|--help" );
            var basicOption = app.Option( "-o|--option <optionvalue>", "Some option value", CommandOptionType.SingleValue );

            app.OnExecute( ( ) => {
                if ( basicOption.HasValue( ) ) {
                    Console.WriteLine( "Option was selected, value: {0}", basicOption.Value( ) );
                } else {
                    app.ShowHint( );
                }

                return 0;
            } );

            app.Command( "update-base", ( command ) => {
                command.Description = "Update the database with the new projects and collaborators";
                command.HelpOption( "-?|-h|--help" );

                command.OnExecute( ( ) => {
                    var response = Actions.UpdateDatabase( );
                    Console.ForegroundColor = ConsoleColor.DarkGreen; ;
                    Console.WriteLine( $"Update database. Inserted {response.newPeoples} collaborators and {response.newProjects} projects." );
                    return 0;
                } );
            } );

            app.Command( "edit", ( command ) => {
                command.Description = "Edit the choosed task";
                command.HelpOption( "-?|-h|--help" );

                var trackOption = command.Option( "-t|--task <task>", "The task to edit.", CommandOptionType.SingleValue );
                var projectOption = command.Option( "-p|--project <project>", "The project where you are working.", CommandOptionType.MultipleValue );
                var collaboratorOption = command.Option( "-c|--collaborator <collaborator>", "Who is working.", CommandOptionType.MultipleValue );
                var extraOption = command.Option( "-x|--extra", "Is hour extra?", CommandOptionType.NoValue );
                var typeEffortOption = command.Option( "-e|--effort", "Type of effort", CommandOptionType.SingleValue );
                var activityOption = command.Option( "-m|--message", "What are you working?", CommandOptionType.MultipleValue );
                var startedOption = command.Option( "-s|--start", "Started at? <date>", CommandOptionType.MultipleValue );
                var endedOption = command.Option( "-f|--finished", "Finished at? <date>", CommandOptionType.MultipleValue );
                var intervallOption = command.Option( "-i|--interval <interval>", "Interval to change time", CommandOptionType.SingleValue );
                var stateOption = command.Option( "-q|--state <state>", "State of interval  [(FINISHED = 0), (RUNNING = 1), (SUBMITED = 3)]", CommandOptionType.SingleValue );

                command.OnExecute( ( ) => {
                    Track track;
                    Project project = null;
                    People people = null;
                    string message = null;
                    TypeOfEffort? typeOfEffort = null;

                    if ( trackOption.HasValue( ) ) {
                        var trackExist = Repository<Track>.Get( x => x.Id == Convert.ToInt32( trackOption.Value( ) ) );
                        if ( trackExist == null && trackExist.Any( ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: The task doesn't exist!" );
                            return 0;
                        }
                        track = trackExist.First( );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The task is required!" );
                        return 0;
                    }

                    if ( projectOption.HasValue( ) ) {
                        var projectExist = Repository<Project>.Get( x => x.Name.Contains( projectOption.Value( ).Trim( ), StringComparison.OrdinalIgnoreCase ) );
                        if ( projectExist == null && projectExist.Any( ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: The project doesn't exist!" );
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine( "TIP: Check if the projects base is up to date!" );
                            return 0;
                        }
                        project = projectExist.First( );
                    }

                    if ( collaboratorOption.HasValue( ) ) {
                        var peopleExist = Repository<People>.Get( x => x.Name == collaboratorOption.Value( ) );
                        if ( peopleExist == null && !peopleExist.Any( ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: The collaborator doesn't exist!" );
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine( "TIP: Check if the employee base is up to date!" );
                            return 0;
                        }
                        people = peopleExist.First( );
                    }

                    if ( activityOption.HasValue( ) ) {
                        message = activityOption.Value( );
                    }

                    if ( typeEffortOption.HasValue( ) ) {
                        switch ( typeEffortOption.Value( ).ToUpper( ) ) {
                            case "0":
                            case "NORMAL":
                                typeOfEffort = TypeOfEffort.NORMAL;
                                break;
                            case "1":
                            case "AJUSTE":
                                typeOfEffort = TypeOfEffort.AJUSTE;
                                break;
                            case "2":
                            case "CORRECAO":
                            case "CORREÇÃO":
                                typeOfEffort = TypeOfEffort.CORRECAO;
                                break;
                            default:
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine( "ERROR: Type of effort not exists!" );
                                return 0;
                        }
                    }

                    DateTime? started = null;
                    if ( startedOption.HasValue( ) ) {
                        try {
                            started = DateTime.ParseExact( startedOption.Value( ), "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture );
                        } catch ( Exception ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: Error on parse ended time" );
                            return 0;
                        }
                    }

                    DateTime? ended = null;
                    if ( endedOption.HasValue( ) ) {
                        try {
                            ended = DateTime.ParseExact( endedOption.Value( ), "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture );
                            if ( started > ended.Value ) {
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine( "ERROR: The started time cannot be greater then ended" );
                                return 1;
                            }
                        } catch ( Exception ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: Error on parse ended time" );
                            return 1;
                        }

                    }

                    int? inter = null;
                    Interval i = null;
                    if ( intervallOption.HasValue( ) ) {
                        track.Intervals = Repository<Interval>.Get( z => z.Track.Id == track.Id ).OrderBy( x => x.Id ).ToList( );
                        inter = Convert.ToInt32( intervallOption.Value( ) ) - 1;
                        if ( inter.Value <= track.Intervals.Count && inter.Value >= 0 ) {
                            i = track.Intervals[ inter.Value ];
                            if ( i.State == State.SUBMITED ) {
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine( "ERROR: This interval has already been submitted and cannot be edited!" );
                                return 1;
                            }
                        } else {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: This interval not exist!" );
                            return 1;
                        }
                    }

                    State? state = null;
                    if ( stateOption.HasValue( ) ) {
                        switch ( stateOption.Value( ).ToUpper( ) ) {
                            case "0":
                            case "FINISHED":
                                state = State.FINISHED;
                                break;
                            case "1":
                            case "RUNNING":
                                state = State.RUNNING;
                                break;
                            case "3":
                            case "SUBMITED":
                                state = State.SUBMITED;
                                break;
                            default:
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine( "ERROR: State do not exist!" );
                                return 1;
                        }
                    }

                    Actions.Edit( track, people, project, extraOption.HasValue( ), typeOfEffort, message, i, started, ended, state );

                    Actions.Print( track );

                    return 0;
                } );
            } );

            app.Command( "task", ( command ) => {
                command.Description = "Include task";
                command.HelpOption( "-?|-h|--help" );
                var projectOption = command.Option( "-p|--project <project>", "The project where you are working.", CommandOptionType.MultipleValue );
                var collaboratorOption = command.Option( "-c|--collaborator <collaborator>", "Who is working.", CommandOptionType.MultipleValue );
                var extraOption = command.Option( "-x|--extra", "Is hour extra?", CommandOptionType.NoValue );
                var typeEffortOption = command.Option( "-e|--effort", "Type of effort", CommandOptionType.SingleValue );
                var activityOption = command.Option( "-m|--message", "What are you working?", CommandOptionType.MultipleValue );
                var startedOption = command.Option( "-s|--start", "Started at?", CommandOptionType.MultipleValue );
                var endedOption = command.Option( "-f|--finish", "Finished at?", CommandOptionType.MultipleValue );
                var noteOption = command.Option( "-n|--note", "Set the notes", CommandOptionType.MultipleValue );

                command.OnExecute( ( ) => {
                    Project project;
                    People people;
                    string message;
                    TypeOfEffort typeOfEffort;

                    if ( projectOption.HasValue( ) ) {
                        var projectExist = Repository<Project>.Get( x => x.Name.Contains( projectOption.Value( ).Trim( ), StringComparison.OrdinalIgnoreCase ) );
                        if ( projectExist == null && !projectExist.Any( ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: The project doesn't exist!" );
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine( "TIP: Check if the project base is up to date!" );
                            return 0;
                        }
                        project = projectExist.First( );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The projetect is required!" );
                        return 0;
                    }

                    people = Repository<People>.Get( x => x.Global ).FirstOrDefault( );
                    if ( collaboratorOption.HasValue( ) ) {
                        var peopleExist = Repository<People>.Get( x => x.Name == String.Join( " ", collaboratorOption.Values.ToArray( ) ) );
                        if ( peopleExist == null && !peopleExist.Any( ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: The collaborator doesn't exist!" );
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine( "TIP: Check if the employee base is up to date!" );
                            return 0;
                        }
                        people = peopleExist.First( );
                    } else if ( people != null && people.Global ) {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine( "WARNING: The global collaborator is being used." );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The collaborator is required!" );
                        return 0;
                    }

                    if ( activityOption.HasValue( ) ) {
                        message = activityOption.Value( );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The message is required!" );
                        return 0;
                    }

                    if ( typeEffortOption.HasValue( ) ) {
                        switch ( typeEffortOption.Value( ).ToUpper( ) ) {
                            case "0":
                            case "NORMAL":
                                typeOfEffort = TypeOfEffort.NORMAL;
                                break;
                            case "1":
                            case "AJUSTE":
                                typeOfEffort = TypeOfEffort.AJUSTE;
                                break;
                            case "2":
                            case "CORRECAO":
                            case "CORREÇÃO":
                                typeOfEffort = TypeOfEffort.CORRECAO;
                                break;
                            default:
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine( "ERROR: Type of effort not exists!" );
                                return 0;
                        }
                    } else {
                        typeOfEffort = TypeOfEffort.NORMAL;
                    }

                    DateTime started = new DateTime( );
                    if ( startedOption.HasValue( ) ) {
                        try {
                            started = DateTime.ParseExact( startedOption.Value( ), "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture );
                        } catch ( Exception ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: Error on parse ended time" );
                            return 0;
                        }
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: Started date is required" );
                        return 0;
                    }

                    DateTime? ended = null;
                    if ( endedOption.HasValue( ) ) {
                        try {
                            ended = DateTime.ParseExact( endedOption.Value( ), "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture );
                            if ( started.CompareTo( ended.Value ) >= 0 ) {
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine( "ERROR: The started time cannot be greater then ended" );
                                return 0;
                            }
                        } catch ( Exception ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: Error on parse ended time" );
                            return 0;
                        }

                    } else {
                        if ( Repository<Interval>.Get( x => x.State == State.RUNNING ).Count( ) > 0 ) {
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine( "TIP: You're already working on a task!" );
                            return 0;
                        }
                    }

                    var notes = new List<Note>( );
                    noteOption.Values.ToList( ).ForEach( x => notes.Add( new Note { Text = x.Trim( ) } ) );

                    Actions.Print( Actions.Create( people, project, extraOption.HasValue( ), typeOfEffort, message, started, ended, notes: notes ) );

                    return 0;
                } );
            } );

            app.Command( "start", ( command ) => {
                command.Description = "Start track time";
                command.HelpOption( "-?|-h|--help" );

                var projectOption = command.Option( "-p|--project <project>", "The project where you are working.", CommandOptionType.MultipleValue );
                var collaboratorOption = command.Option( "-c|--collaborator <collaborator>", "Who is working.", CommandOptionType.MultipleValue );
                var extraOption = command.Option( "-x|--extra", "Is hour extra?", CommandOptionType.NoValue );
                var typeEffortOption = command.Option( "-e|--effort", "Type of effort", CommandOptionType.SingleValue );
                var activityOption = command.Option( "-m|--message", "What are you working?", CommandOptionType.MultipleValue );
                var noteOption = command.Option( "-n|--note", "Set the notes", CommandOptionType.MultipleValue );

                command.OnExecute( ( ) => {

                    if ( Repository<Interval>.Get( x => x.State == State.RUNNING ).Any( ) ) {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine( "TIP: You're already working on a task!" );
                        return 0;
                    }

                    Project project;
                    People people;
                    string message;
                    TypeOfEffort typeOfEffort;

                    if ( projectOption.HasValue( ) ) {
                        var projectExist = Repository<Project>.Get( x => x.Name.Contains( projectOption.Value( ).Trim( ), StringComparison.OrdinalIgnoreCase ) );
                        if ( projectExist == null && !projectExist.Any( ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: The project doesn't exist!" );
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine( "TIP: Check if the employee base is up to date!" );
                            return 0;
                        }
                        project = projectExist.First( );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The projetect is required!" );
                        return 0;
                    }

                    people = Repository<People>.Get( x => x.Global ).FirstOrDefault( );
                    if ( collaboratorOption.HasValue( ) ) {
                        var peopleExist = Repository<People>.Get( x => x.Name == String.Join( " ", collaboratorOption.Values.ToArray( ) ) );
                        if ( peopleExist == null && !peopleExist.Any( ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: The collaborator doesn't exist!" );
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine( "TIP: Check if the employee base is up to date!" );
                            return 0;
                        }
                        people = peopleExist.First( );
                    } else if ( people != null && people.Global ) {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine( "WARNING: The global collaborator is being used." );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The collaborator is required!" );
                        return 0;
                    }

                    if ( activityOption.HasValue( ) ) {
                        message = activityOption.Value( );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The message is required!" );
                        return 0;
                    }

                    if ( typeEffortOption.HasValue( ) ) {
                        switch ( typeEffortOption.Value( ).ToUpper( ) ) {
                            case "0":
                            case "NORMAL":
                                typeOfEffort = TypeOfEffort.NORMAL;
                                break;
                            case "1":
                            case "AJUSTE":
                                typeOfEffort = TypeOfEffort.AJUSTE;
                                break;
                            case "2":
                            case "CORRECAO":
                            case "CORREÇÃO":
                                typeOfEffort = TypeOfEffort.CORRECAO;
                                break;
                            default:
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine( "ERROR: Type of effort not exists!" );
                                return 0;
                        }
                    } else {
                        typeOfEffort = TypeOfEffort.NORMAL;
                    }

                    var notes = new List<Note>( );
                    noteOption.Values.ToList( ).ForEach( x => notes.Add( new Note { Text = x.Trim( ) } ) );

                    Actions.Print( Actions.Create( people, project, extraOption.HasValue( ), typeOfEffort, message, notes: notes ) );

                    return 0;
                } );
            } );

            app.Command( "stop", ( command ) => {
                command.Description = "Stop the running task";
                command.HelpOption( "-?|-h|--help" );

                //var trackId = command.Option( "-t|--task <id>", "The track do you want pause.", CommandOptionType.SingleValue );
                command.OnExecute( ( ) => {

                    var tracks = Repository<Track>.Get( ).ToList( );
                    tracks.ToList( ).ForEach( x => {
                        x.Intervals = Repository<Interval>.Get( z => z.Track.Id == x.Id ).ToList( );
                    } );
                    var trackId = tracks.SingleOrDefault( x => x.Intervals.Any( z => z.State == State.RUNNING ) );

                    if ( trackId != null ) {
                        var response = Actions.ChangeState( trackId.Id, State.FINISHED );
                        if ( response.sucess ) {
                            Actions.Print( response.track );
                        }
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: No tasks are running" );
                    }
                    return 0;
                } );
            } );

            app.Command( "resume", ( command ) => {
                command.Description = "Resume the choosed task";
                command.HelpOption( "-?|-h|--help" );

                var trackId = command.Option( "-t|--task <id>", "The track do you want resume.", CommandOptionType.SingleValue );
                var extraHour = command.Option( "-x|--extra", "Extra hour.", CommandOptionType.NoValue );

                command.OnExecute( ( ) => {

                    if ( Repository<Interval>.Get( x => x.State == State.RUNNING ).Any( ) ) {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine( "TIP: You're already working on a task!" );
                        return 0;
                    }

                    var response = Actions.ChangeState( Convert.ToInt32( trackId.Value( ) ), State.RUNNING, extraHour.HasValue( ) );
                    if ( response.sucess ) {
                        Actions.Print( response.track );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine( "TIP: This track is runing yet" );
                    }
                    return 0;
                } );
            } );

            app.Command( "list", ( command ) => {
                command.Description = "List the tasks";
                command.HelpOption( "-?|-h|--help" );

                var countOption = command.Option( "-a|--amount <amount>", "Number of tasks.", CommandOptionType.SingleValue );
                var collaboratorOption = command.Option( "-c|--collaborators", "List collaborators name", CommandOptionType.NoValue );
                var projectsOption = command.Option( "-p|--projects", "List collaborators name", CommandOptionType.NoValue );
                var intervalsOption = command.Option( "-i|--intervals", "Expand list to show intervals", CommandOptionType.NoValue );

                command.OnExecute( ( ) => {
                    var amount = 0;

                    if ( countOption.HasValue( ) ) {
                        if ( String.IsNullOrEmpty( countOption.Value( ) ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: Amount can not be empty!" );
                        } else {
                            amount = Convert.ToInt32( countOption.Value( ) );
                        }
                    }

                    if ( !collaboratorOption.HasValue( ) && !projectsOption.HasValue( ) ) {
                        var tracks = Repository<Track>.Get( );
                        amount = amount == 0 ? tracks.Count( ) : amount;

                        if ( amount > 0 )
                            Actions.Print( tracks.TakeLast( amount ).ToList( ), intervals: intervalsOption.HasValue( ) );
                        else {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: No tasks found" );
                        }
                    } else if ( collaboratorOption.HasValue( ) ) {
                        var collab = Repository<People>.Get( );
                        amount = collab.Count( );

                        if ( amount > 0 ) {
                            var a = Actions.SplitList( collab.ToList( ), 4 );

                            var result = new List<People>( );
                            Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                            Console.WriteLine( "{0,50} | {1,50} | {1,50} | {1,50}", "", "", "", "" );
                            foreach ( var i in a ) {
                                foreach ( var j in i )
                                    Console.Write( "{0,50} | ", j.Name );
                                Console.WriteLine( );
                            }
                            Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                        }

                    } else if ( projectsOption.HasValue( ) ) {
                        var projects = Repository<Project>.Get( );
                        amount = projects.Count( );

                        if ( amount > 0 ) {
                            var a = Actions.SplitList( projects.ToList( ), 4 );

                            var result = new List<People>( );
                            Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                            Console.WriteLine( "{0,50} | {1,50} | {1,50} | {1,50}", "", "", "", "" );
                            foreach ( var i in a ) {
                                foreach ( var j in i )
                                    Console.Write( "{0,50} | ", j.Name );
                                Console.WriteLine( );
                            }
                            Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                        }

                    }
                    return 0;
                } );
            } );

            app.Command( "time", ( command ) => {
                command.Description = "Show total time by projects";
                command.HelpOption( "-?|-h|--help" );

                var dayOption = command.Option( "-d|--day <day>", "Day of work.", CommandOptionType.SingleValue );
                var monthOption = command.Option( "-m|--month <month>", "Month of work.", CommandOptionType.SingleValue );
                var yearOption = command.Option( "-y|--year <year>", "Year of work.", CommandOptionType.SingleValue );

                command.OnExecute( ( ) => {
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine( "{0,-24} | {1,-23} | {2,-20} |", "YEAR", "MONTH", "DAY" );
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                    Console.WriteLine( "{0,-24} | {1,-23} | {2,-20} |", ( yearOption.HasValue( ) ? yearOption.Value( ) : "" ), ( monthOption.HasValue( ) ? monthOption.Value( )  : "" ), ( dayOption.HasValue( ) ? dayOption.Value( ) : "" ) );
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine( "{0,-50} | {1,-20} | ", "PROJECT", "TOTAL TIME" );
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                    foreach ( var i in Actions.TotalTime( yearOption.HasValue( ) ? Convert.ToInt32(yearOption.Value( )) : 0, monthOption.HasValue( ) ? Convert.ToInt32( monthOption.Value( ) ) : 0, dayOption.HasValue( ) ? Convert.ToInt32( dayOption.Value( ) ) : 0 ) ) {
                        Console.WriteLine( "{0,-50} | {1,-20} |", i.project, Actions.TimeSpanToTime( i.time ) );
                    }
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );

                    return 0;
                } );
            } );

            app.Command( "submit", ( command ) => {
                command.Description = "Submit all finished tracks";
                command.HelpOption( "-?|-h|--help" );

                var visaulOption = command.Option( "-v|--visual", "Show the browser on submit.", CommandOptionType.NoValue );
                var taskOption = command.Option( "-t|--task <task>", "How task to submit.", CommandOptionType.MultipleValue );

                command.OnExecute( ( ) => {

                    var tracks = Repository<Track>.Get( ).ToList( );
                    tracks.ForEach( x => {
                        x.Intervals = Repository<Interval>.Get( z => ( z.Track.Id == x.Id ) ).ToList( );
                        x.Project = Repository<Project>.GetById( x.Project.Id );
                        x.Collaborator = Repository<People>.GetById( x.Collaborator.Id );
                    } );
                    tracks = tracks.Where( x => x.Intervals.Any() && x.Intervals.Last( ).State != State.RUNNING ).ToList( );

                    if ( taskOption.HasValue( ) ) {
                        tracks = tracks.Where( x => taskOption.Values.Select( int.Parse ).ToList( ).Contains( x.Id ) ).ToList( );
                    }

                    Selenium s = new Selenium( visaulOption.HasValue( ) );
                    foreach ( var t in tracks ) {
                        s.Submit( t );
                    }
                    s.Close( );
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine( "SUCCESS: All tasks have been submitted!" );
                    return 0;
                } );
            } );

            app.Command( "global", ( command ) => {
                command.Description = "Set global settings";
                command.HelpOption( "-?|-h|--help" );

                var collaboratorOption = command.Option( "-c|--collaborator", "Set the global collaborator.", CommandOptionType.SingleValue );

                command.OnExecute( ( ) => {
                    if ( collaboratorOption.HasValue( ) ) {
                        var col = Repository<People>.Get( x => x.Name == collaboratorOption.Value( ) ).FirstOrDefault( );
                        if ( col != null ) {
                            var cols = Repository<People>.Get( );
                            cols.ToList( ).ForEach( x => {
                                x.Global = false;
                                Repository<People>.Update( x );
                            } );
                            col.Global = true;
                            Repository<People>.Update( col );
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                            Console.WriteLine( "SUCCESS: Global collaborator updated!" );
                            return 0;
                        } else {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: No collaborator with this name were found" );
                        }
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: Collaborator's name is required!" );
                    }
                    return 1;
                } );

            } );

            app.Command( "todo", ( command ) => {
                command.Description = "Create task to do later";
                command.HelpOption( "-?|-h|--help" );

                var collaboratorOption = command.Option( "-c|--collaborator", "Set the collaborator", CommandOptionType.SingleValue );
                var projectOption = command.Option( "-p|--project", "Set the project", CommandOptionType.SingleValue );
                var messageOption = command.Option( "-m|--message", "Set the message", CommandOptionType.SingleValue );
                var typeEffortOption = command.Option( "-e|--effort", "Set the effort", CommandOptionType.SingleValue );
                var noteOption = command.Option( "-n|--note", "Set the notes", CommandOptionType.MultipleValue );

                command.OnExecute( ( ) => {
                    Project project;
                    People people;
                    string message;
                    TypeOfEffort typeOfEffort;

                    if ( projectOption.HasValue( ) ) {
                        var projectExist = Repository<Project>.Get( x => x.Name.Contains( projectOption.Value( ).Trim( ), StringComparison.OrdinalIgnoreCase ) );
                        if ( projectExist == null && !projectExist.Any( ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: The project doesn't exist!" );
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine( "TIP: Check if the employee base is up to date!" );
                            return 0;
                        }
                        project = projectExist.First( );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The projetect is required!" );
                        return 0;
                    }

                    people = Repository<People>.Get( x => x.Global ).FirstOrDefault( );
                    if ( collaboratorOption.HasValue( ) ) {
                        var peopleExist = Repository<People>.Get( x => x.Name == String.Join( " ", collaboratorOption.Values.ToArray( ) ) );
                        if ( peopleExist == null && !peopleExist.Any( ) ) {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine( "ERROR: The collaborator doesn't exist!" );
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine( "TIP: Check if the employee base is up to date!" );
                            return 0;
                        }
                        people = peopleExist.First( );
                    } else if ( people != null && people.Global ) {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine( "WARNING: The global collaborator is being used." );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The collaborator is required!" );
                        return 0;
                    }

                    if ( messageOption.HasValue( ) ) {
                        message = messageOption.Value( );
                    } else {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine( "ERROR: The message is required!" );
                        return 0;
                    }

                    if ( typeEffortOption.HasValue( ) ) {
                        switch ( typeEffortOption.Value( ).ToUpper( ) ) {
                            case "0":
                            case "NORMAL":
                                typeOfEffort = TypeOfEffort.NORMAL;
                                break;
                            case "1":
                            case "AJUSTE":
                                typeOfEffort = TypeOfEffort.AJUSTE;
                                break;
                            case "2":
                            case "CORRECAO":
                            case "CORREÇÃO":
                                typeOfEffort = TypeOfEffort.CORRECAO;
                                break;
                            default:
                                Console.ForegroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine( "ERROR: Type of effort not exists!" );
                                return 0;
                        }
                    } else {
                        typeOfEffort = TypeOfEffort.NORMAL;
                    }

                    var notes = new List<Note>( );
                    noteOption.Values.ToList( ).ForEach( x => notes.Add( new Note { Text = x.Trim( ) } ) );

                    Actions.Print( Actions.Create( people, project, false, typeOfEffort, message, state: State.TODO, notes: notes ) );

                    return 0;
                } );

            } );

            app.Command( "note", ( command ) => {
                command.Description = "Add note to task";
                command.HelpOption( "-?|-h|--help" );

                var task = command.Option( "-t|--task", "The task to add", CommandOptionType.SingleValue );
                var noteOption = command.Option( "-n|--notes", "The notes", CommandOptionType.MultipleValue );

                command.OnExecute( ( ) => {

                    var track = Repository<Track>.GetById( Convert.ToInt32( task.Value( ) ) );

                    var notes = new List<Note>( );
                    noteOption.Values.ToList( ).ForEach( x => notes.Add( new Note { Text = x.Trim( ) } ) );

                    Actions.AddNotes( track, notes );
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine( "SUCCESS: Notes added!" );
                    return 1;
                } );
            } );

            app.Command( "show", ( command ) => {
                command.Description = "Add note to task";
                command.HelpOption( "-?|-h|--help" );

                var task = command.Option( "-t|--task", "The task to add", CommandOptionType.SingleValue );

                command.OnExecute( ( ) => {
                    var track = Repository<Track>.GetById( Convert.ToInt32( task.Value( ) ) );
                    track.Project = Repository<Project>.GetById( track.Project.Id );
                    track.Collaborator = Repository<People>.GetById( track.Collaborator.Id );
                    track.Intervals = Repository<Interval>.Get( z => z.Track.Id == track.Id ).ToList( );
                    track.Notes = Repository<Note>.Get( z => z.Track.Id == track.Id ).ToList( );

                    string format = "{0,-10} | {1,-40} | {2,-25} | {3,-60} | {4,30} | {5,20} | {6,20}";
                    string formatPartial = "{0,-10} | {1,-40} |{2,-39}|{3,10}|{4,10} | {5,25} | {6,30} | {7,20} | {8,20}";


                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine( format, "ID", "PROJECT", "COLLABORATOR", "MESSAGE", "EFFORT", "TOTAL TIME", "LAST STATE" );
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine( formatPartial, "", "", "", "INTERVAL", "EXTRA", "RESUMED ON", "PAUSED ON", "PARTIAL TIME", "STATE" );
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                    TimeSpan? total = new TimeSpan( );
                    var i = 1;
                    if ( track.Intervals.Any( ) )
                        foreach ( var date in track.Intervals ) {
                            if ( date.End.HasValue ) {
                                    Console.WriteLine( formatPartial, "", "", "", $"{i}", date.ExtraHour ? "YES" : "NO", date.Start.ToString( "dd/MM/yyyy - HH:mm:ss" ), date.End.Value.ToString( "dd/MM/yyyy - HH:mm:ss" ), ( date.End - date.Start ).Value.ToString( @"hh\:mm\:ss" ), date.State );
                                total += ( date.End - date.Start );
                            } else {
                                Console.WriteLine( formatPartial, "", "", "", $"{i}", date.ExtraHour ? "YES" : "NO", date.Start.ToString( "dd/MM/yyyy - HH:mm:ss" ), "", ( DateTime.Now - date.Start ).ToString( @"hh\:mm\:ss" ), date.State );
                                total += ( DateTime.Now - date.Start );
                            }
                            i++;
                        }
                    Console.ForegroundColor = ConsoleColor.White;
                    if ( track.Intervals.Any( ) ) {
                        var last = track.Intervals.Last( );
                        if ( last.End.HasValue )
                            Console.WriteLine( format, track.Id, ( track.Project.Name.Length > 40 ? new String( track.Project.Name.Take( 37 ).ToArray( ) ) + "..." : track.Project.Name ), ( track.Collaborator.Name.Length > 40 ? new String( track.Collaborator.Name.Take( 37 ).ToArray( ) ) + "..." : track.Collaborator.Name ), ( track.Activity.Length > 60 ? new String( track.Activity.Take( 57 ).ToArray( ) ) + "..." : track.Activity ), track.TypeOfEffort, total.Value.ToString( @"hh\:mm\:ss" ), last.State );
                        else
                            Console.WriteLine( format, track.Id, ( track.Project.Name.Length > 40 ? new String( track.Project.Name.Take( 37 ).ToArray( ) ) + "..." : track.Project.Name ), ( track.Collaborator.Name.Length > 40 ? new String( track.Collaborator.Name.Take( 37 ).ToArray( ) ) + "..." : track.Collaborator.Name ), ( track.Activity.Length > 60 ? new String( track.Activity.Take( 57 ).ToArray( ) ) + "..." : track.Activity ), track.TypeOfEffort, total.Value.ToString( @"hh\:mm\:ss" ), last.State );
                    } else
                        Console.WriteLine( format, track.Id, ( track.Project.Name.Length > 40 ? new String( track.Project.Name.Take( 37 ).ToArray( ) ) + "..." : track.Project.Name ), ( track.Collaborator.Name.Length > 40 ? new String( track.Collaborator.Name.Take( 37 ).ToArray( ) ) + "..." : track.Collaborator.Name ), ( track.Activity.Length > 60 ? new String( track.Activity.Take( 57 ).ToArray( ) ) + "..." : track.Activity ), track.TypeOfEffort, "", "", "TO DO" );
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine( "{0, -22} | {1, -200}", "CREATED AT", "NOTE" );
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                    foreach (var note in track.Notes ) {
                        Console.WriteLine( "{0, -22} | {1, -200}", note.CreatedAt.ToString( "dd/MM/yyyy - HH:mm:ss" ), note.Text );
                    }
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );

                    return 1;
                } );
            } );

            try {
                app.Execute( args );
            } catch ( Exception e ) {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine( "ERROR: On execute command: " + e.Message );
            }
        }
    }
}

//TODO: Verificar se maior q meia noite