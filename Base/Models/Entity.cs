﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Models {
    public class Entity {
        public virtual int Id { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
    }
}
