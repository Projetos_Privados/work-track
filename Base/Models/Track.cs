﻿using System;
using System.Collections.Generic;
using System.Text;
using static Base.Models.Enumerations;

namespace Base.Models {
    public class Track : Entity{
        public People Collaborator { get; set; }
        public Project Project { get; set; }
        public string Activity { get; set; }
        public TypeOfEffort TypeOfEffort { get; set; }
        public List<Interval> Intervals { get; set; }
        public List<Note> Notes { get; set; }

        public Track( ) {
            Intervals = new List<Interval>( );
            Notes = new List<Note>( );
        }
    }
}
