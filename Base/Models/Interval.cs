﻿using System;
using System.Collections.Generic;
using System.Text;
using static Base.Models.Enumerations;

namespace Base.Models {
    public class Interval : Entity {
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public bool ExtraHour { get; set; }
        public State State { get; set; }
        public DateTime? SubmitedAt { get; set; }
        public Track Track { get; set; }
    }
}
