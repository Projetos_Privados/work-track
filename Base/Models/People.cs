﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Models {
    public class People : Entity {
        public string Name { get; set; }
        public bool Global { get; set; }
    }
}
