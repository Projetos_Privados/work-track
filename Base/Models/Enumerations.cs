﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Models {
    public class Enumerations {
        public enum TypeOfEffort { NORMAL = 0, AJUSTE = 1, CORRECAO = 2 }
        public enum State { FINISHED = 0, RUNNING = 1, TODO = 2, SUBMITED = 3 }
    }
}
