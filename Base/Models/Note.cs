﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Models {
    public class Note: Entity {
        public string Text { get; set; }
        public Track Track { get; set; }

        public Note( ) {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }
    }
}
