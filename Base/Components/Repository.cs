﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Base.Components.Utils;
using Base.Models;

namespace Base.Components {
    public class Repository<T> where T: Entity {
        public static T Add( T value ) {
            using(var db = new LiteDatabase( Paths.Database ) ) {
                value.CreatedAt = DateTime.Now;
                value.UpdatedAt = DateTime.Now;
                db.GetCollection<T>( ).Insert( value );
            }
            return value;
        }

        public static T Update (T value ) {
            using(var db = new LiteDatabase( Paths.Database ) ) {
                value.UpdatedAt = DateTime.Now;
                db.GetCollection<T>( ).Update( value );
            }
            return value;
        }

        public static IEnumerable<T> Get ( Func<T, bool> filter = null ) {
            using(var db = new LiteDatabase( Paths.Database ) ) {
                var all = db.GetCollection<T>( ).FindAll( ).OrderBy( x => x.Id);
                if ( filter == null )
                    return all;
                return all.Where( filter );
            }
        }

        public static T GetById (int id ) {
            using(var db = new LiteDatabase( Paths.Database ) ) {
                return db.GetCollection<T>( ).FindById( id );
            }
        }
        
    }
}
