﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Base.Components.Extensions;
using Base.Models;

using static Base.Models.Enumerations;
using Base.Components.Utils;

namespace Base.Components {
    public class Selenium {

        private IWebDriver _driver;
        private const string _URL = "https://docs.google.com/forms/d/e/1FAIpQLSdNy__W8kenU7rN2BZagWo6xDOqED-3lDOBAm6j46gzGaJ20g/viewform";

        public Selenium( bool visual = false ) {
            var chromeOptions = new ChromeOptions( );
            chromeOptions.AddArgument( "--lang=en-US" );
            if ( visual ) {
                _driver = new ChromeDriver( Paths.WebDrivePath, chromeOptions ) {
                    Url = _URL
                };
            } else {
                chromeOptions.AddArguments( "headless" );
                chromeOptions.AddArgument( "--log-level=3" );

                ChromeDriverService service = ChromeDriverService.CreateDefaultService( Paths.WebDrivePath );
                service.HideCommandPromptWindow = true;

                _driver = new ChromeDriver( service, chromeOptions ) {
                    Url = _URL
                };
            }
            _driver.Manage( ).Timeouts( ).ImplicitWait = TimeSpan.FromSeconds( 5 );
        }

        public void Close( ) {
            _driver.Quit( );
        }

        public List<string> GetCollaborators( ) {
            var response = new List<string>( );
            var countCollaborators = _driver.FindElements( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[1]/div/div[2]/div[1]/div[1]/div" ) );
            for ( int i = 3; i < countCollaborators.Count; i++ )
                response.Add( _driver.FindElement( By.XPath( $"//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[1]/div/div[2]/div[1]/div[1]/div[{i}]/span" ) ).GetAttribute( "innerText" ) );
            return response;
        }

        public List<string> GetProjects( ) {
            var response = new List<string>( );
            var countProjects = _driver.FindElements( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[1]/div" ) );
            for ( int i = 3; i < countProjects.Count; i++ )
                response.Add( _driver.FindElement( By.XPath( $"//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[1]/div[{i}]/span" ) ).GetAttribute( "innerText" ) );
            return response;
        }

        public void Submit( Track track ) {
            var countCollaborators = _driver.FindElements( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[1]/div/div[2]/div[1]/div[1]/div" ) );
            foreach ( var inter in track.Intervals.Where( x => ( x.State == State.FINISHED ) ).ToList( ) ) {

                #region [ Collaborator ]
                var select = _driver.FindElements( By.ClassName( "quantumWizMenuPaperselectOptionList" ), 10 );
                select[ 0 ].Click( );
                var options = _driver.FindElements( By.ClassName( "exportSelectPopup" ) )[ 0 ];
                var contents = options.FindElements( By.TagName( "div" ) );
                foreach ( var i in contents ) {
                    if ( i.Text == track.Collaborator.Name ) {
                        i.Click( );
                        break;
                    }
                }
                #endregion

                Thread.Sleep( 500 );

                #region [ Project ]
                select = _driver.FindElements( By.ClassName( "quantumWizMenuPaperselectOptionList" ), 10 );
                select[ 1 ].Click( );
                options = _driver.FindElements( By.ClassName( "exportSelectPopup" ) )[ 1 ];
                contents = options.FindElements( By.TagName( "div" ) );
                foreach ( var i in contents ) {
                    if ( i.Text == track.Project.Name ) {
                        i.Click( );
                        Thread.Sleep( 500 );
                        break;
                    }
                }
                #endregion

                #region [ Date ]
                _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[3]/div/div[2]/div/div[2]/div[1]/div/div[1]/input" ) ).SendKeys( inter.Start.ToString( "MM-dd-yyyy" ) );
                #endregion

                #region [ Effort ]
                _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[4]/div/div[2]/div[1]/div[2]/div[1]/div/div[1]/input" ) ).SendKeys( ( inter.End - inter.Start ).Value.ToString( "hh" ) );
                _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[4]/div/div[2]/div[3]/div[2]/div[1]/div/div[1]/input" ) ).SendKeys( ( inter.End - inter.Start ).Value.ToString( "mm" ) );
                _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[4]/div/div[2]/div[5]/div[2]/div[1]/div/div[1]/input" ) ).SendKeys( ( inter.End - inter.Start ).Value.ToString( "ss" ) );
                #endregion

                #region [ Activity ]
                _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[5]/div/div[2]/div[1]/div[2]/textarea" ) ).SendKeys( track.Activity );
                #endregion

                #region [ Extra Hour ]
                if ( !inter.ExtraHour )
                    _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[6]/div/div[2]/div/span/div/div[1]/label/div" ) ).Click( );
                else
                    _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[6]/div/div[2]/div/span/div/div[2]/label/div" ) ).Click( );
                #endregion

                #region [ Type of Effort ]
                switch ( track.TypeOfEffort ) {
                    case TypeOfEffort.NORMAL:
                        _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[7]/div/div[2]/div/span/div/div[1]/label/div" ) ).Click( );
                        break;
                    case TypeOfEffort.AJUSTE:
                        _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[7]/div/div[2]/div/span/div/div[2]/label/div" ) ).Click( );
                        break;
                    case TypeOfEffort.CORRECAO:
                        _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[2]/div[7]/div/div[2]/div/span/div/div[3]/label/div" ) ).Click( );
                        break;
                }
                #endregion

                #region [ Submit ]
#if RELEASE
                _driver.FindElement( By.XPath( "//*[@id=\"mG61Hd\"]/div/div[2]/div[3]/div[1]/div/div" ) ).Click( );
#endif
                #endregion

                #region [ Update database ]
                inter.SubmitedAt = DateTime.Now;
                inter.State = State.SUBMITED;
                Repository<Interval>.Update( inter );
                #endregion

                Thread.Sleep( 1000 );
                _driver.Navigate( ).GoToUrl( _URL );
#if DEBUG
                IAlert alert = _driver.SwitchTo( ).Alert( );
                alert.Accept( );
#endif
            }
        }
    }
}
