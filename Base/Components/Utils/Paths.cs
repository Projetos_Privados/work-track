﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Base.Components.Utils {
    class Paths {

        public static string WebDrivePath {
            get {
                return Directory.GetCurrentDirectory( );
            }
        }

        public static string Database {
            get {
                var dir = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ), "WorkTrack" );
                if ( !Directory.Exists( dir ) ) {
                    Directory.CreateDirectory( dir );
                }

#if RELEASE
                if ( RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) ) return $"Filename={dir}//data.litedb; Mode=Exclusive";
                else if ( RuntimeInformation.IsOSPlatform( OSPlatform.OSX ) ) return $"Filename={dir}//data.litedb; Mode=Exclusive";
                return $"{dir}//data.litedb";
#else
                if ( RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) ) return "Filename=data.litedb; Mode=Exclusive";
                else if ( RuntimeInformation.IsOSPlatform( OSPlatform.OSX ) ) return "Filename=data.litedb; Mode=Exclusive";
                return "data.litedb";
#endif
            }
        }
    }
}
