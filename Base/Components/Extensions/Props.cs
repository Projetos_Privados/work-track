﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.Components.Extensions {
    public static class Props {
        public static void CopyTo<T, TU>( this T source, TU dest, bool setId = false) {
            var sourceProps = typeof( T ).GetProperties( ).Where( x => x.CanRead ).ToList( );
            var destProps = typeof( TU ).GetProperties( )
                    .Where( x => x.CanWrite )
                    .ToList( );

            foreach ( var sourceProp in sourceProps ) {
                if ( destProps.Any( x => x.Name == sourceProp.Name ) ) {
                    var p = destProps.First( x => x.Name == sourceProp.Name );
                    if ( p.CanWrite && (!setId && p.Name != "Id") ) { // check if the property can be set or no.
                        p.SetValue( dest, sourceProp.GetValue( source, null ), null );
                    }
                }

            }

        }
    }
}
