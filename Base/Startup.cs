﻿using LiteDB;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Base.Components.Utils;
using Base.Models;

namespace Base {
    public class Startup {
        private static void Database( ) {
            var mapper = BsonMapper.Global;
            
            mapper.Entity<People>( )
                .Id( x => x.Id );

            mapper.Entity<Project>( )
                .Id( x => x.Id );

            mapper.Entity<Track>( )
                .Id( x => x.Id )
                .DbRef( x => x.Project, "Project" )
                .DbRef( x => x.Collaborator, "People" )
                .DbRef( x => x.Intervals, "Interval" )
                .DbRef( x => x.Notes, "Note" );

            mapper.Entity<Interval>( )
                .Id( x => x.Id )
                .DbRef( x => x.Track, "Track" );

            mapper.Entity<Note>( )
                .Id( x => x.Id );

            mapper.IncludeFields = true;
            
        }

        public static void Init( ) {
            CultureInfo.CurrentCulture = new CultureInfo( "en-US", false );
            Database( );
        }
    }
}
