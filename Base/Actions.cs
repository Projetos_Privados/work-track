﻿using Base.Components;
using Base.Components.Utils;
using Base.Models;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using static Base.Models.Enumerations;

namespace Base {
    public class Actions {
        public static (int newPeoples, int newProjects) UpdateDatabase( ) {
            Selenium s = new Selenium( );
            var externalPeoples = s.GetCollaborators( );
            var externalProjects = s.GetProjects( );
            s.Close( );
            using ( var db = new LiteDatabase( Paths.Database ) ) {
                var peoples = db.GetCollection<People>( );
                var localPeoples = peoples.FindAll( ).Select( x => x.Name );
                var newsPeples = externalPeoples.Where( x => !localPeoples.Any( z => z == x ) ).ToList( );
                foreach ( var np in newsPeples ) {
                    peoples.Insert( new People { Name = np, CreatedAt = DateTime.Now } );
                }

                var projects = db.GetCollection<Project>( );
                var localProjects = projects.FindAll( ).Select( x => x.Name );
                var newsProj = externalProjects.Where( x => !localProjects.Any( z => z == x ) ).ToList( );
                foreach ( var np in newsProj ) {
                    projects.Insert( new Project { Name = np, CreatedAt = DateTime.Now } );
                }
                return (newsPeples.Count, newsProj.Count);
            }
        }

        public static Track Edit( Track track, People? collaborator, Project? project, bool? extraHour, TypeOfEffort? typeOfEffort, string activity, Interval inter, DateTime? started, DateTime? ended, State? state ) {
            track.UpdatedAt = DateTime.Now;
            if ( !string.IsNullOrEmpty( activity ) )
                track.Activity = activity;

            if ( collaborator != null )
                track.Collaborator = collaborator;


            if ( project != null )
                track.Project = project;

            if ( typeOfEffort != null )
                track.TypeOfEffort = typeOfEffort.Value;

            if(inter != null ) {
                if ( started.HasValue ) {
                    inter.Start = started.Value;
                }

                if ( ended.HasValue ) {
                    inter.End = ended.Value;
                    inter.State = State.FINISHED;
                }

                if ( extraHour.HasValue )
                    inter.ExtraHour = extraHour.Value;

                if ( state.HasValue ) {
                    inter.State = state.Value;
                }

                Repository<Interval>.Update( inter );
            }

            Repository<Track>.Update( track );
            return track;
        }

        public static Track Create( People collaborator, Project project, bool extraHour, TypeOfEffort typeOfEffort, string activity, DateTime? started = null, DateTime? ended = null, State? state = null, List<Note> notes = null ) {
            Track track = new Track {
                Activity = activity,
                Collaborator = collaborator,
                CreatedAt = DateTime.Now,
                Project = project,
                TypeOfEffort = typeOfEffort,
            };
            Repository<Track>.Add( track );

            if ( (state.HasValue && state.Value != State.TODO) || !state.HasValue ) {

                Interval interval = new Interval {
                    Start = started ?? DateTime.Now,
                    End = ended,
                    ExtraHour = extraHour,
                    State = ended.HasValue ? State.FINISHED : State.RUNNING,
                    Track = track
                };
                Repository<Interval>.Add( interval );

                track.Intervals.Add( interval );
            }

            if( notes != null ) {
                notes.ForEach( x => {
                    x.Track = track;
                    Repository<Note>.Add( x );
                    track.Notes.Add( x );
                } );
            }

            return track;
        }

        public static (bool sucess, Track track) ChangeState( int trackId, State state, bool extraHour = false ) {
            var track = Repository<Track>.GetById( trackId );
            track.Intervals = Repository<Interval>.Get( x => x.Track.Id == track.Id ).ToList( );
            Interval last = null;
            if ( state == State.FINISHED ) {
                last = track.Intervals.LastOrDefault( x => x.State == State.RUNNING );
                if ( last != null ) {
                    last.State = State.FINISHED;
                    if( DateTime.Now.Day > last.Start.Day ) {
                        last.End = new DateTime(last.Start.Year, last.Start.Month, last.Start.Day, 23, 59, 59);
                        var inter = new Interval {
                            Start = new DateTime( DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00),
                            End = DateTime.Now,
                            Track = track,
                            State = State.FINISHED,
                            ExtraHour = last.ExtraHour,
                        };
                        Repository<Interval>.Add( inter );
                    } else {
                        last.End = DateTime.Now;
                    }
                    Repository<Interval>.Update( last );
                    return (true, track);
                }
            } else if ( state == State.RUNNING ) {
                last = track.Intervals.LastOrDefault( x => x.State == State.FINISHED || x.State == State.SUBMITED );
                if ( last != null ) {
                    var inter = new Interval {
                        Start = DateTime.Now,
                        State = State.RUNNING,
                        ExtraHour = extraHour,
                        Track = track
                    };
                    Repository<Interval>.Add( inter );
                    return (true, track);
                } else {
                    Interval interval = new Interval {
                        Start = DateTime.Now,
                        ExtraHour = extraHour,
                        State = State.RUNNING,
                        Track = track
                    };
                    Repository<Interval>.Add( interval );
                    return (true, track);
                }
            }
            return (false, track);
        }

        public static List<(string project, TimeSpan time)> TotalTime(int? year = null, int? month = null, int? day = null ) {
            var tracks = Repository<Track>.Get( ).ToList( );
            tracks.ForEach( x => {
                x.Intervals = Repository<Interval>.Get( z => ( z.Track.Id == x.Id ) ).ToList( );
                x.Project = Repository<Project>.GetById( x.Project.Id );
                x.Collaborator = Repository<People>.GetById( x.Collaborator.Id );
            } );

            List<(string project, TimeSpan time)> total = new List<(string project, TimeSpan time)>( );
            var hash = new Dictionary<string, List<Track>>( );

            foreach ( var track in tracks ) {
                if ( hash.ContainsKey( track.Project.Name ) ) {
                    hash[ track.Project.Name ].Add( track );
                } else {
                    hash.Add( track.Project.Name, new List<Track>( ) );
                }
            }

            foreach ( var key in hash.Keys ) {
                TimeSpan t = TimeSpan.Zero;
                foreach ( var track in hash[ key ] ) {
                    foreach ( var inter in track.Intervals ) {
                        if ( ( ( year != 0 && inter.Start.Year == year ) || year == 0 ) &&
                            ( ( month != 0 && inter.Start.Month == month ) || month == 0 ) &&
                            ( ( day != 0 && inter.Start.Day == day ) || day == 0 )  ) {
                            if ( inter.End.HasValue )
                                t += inter.End.Value - inter.Start;
                            else
                                t += DateTime.Now - inter.Start;
                        }
                    }
                }
                total.Add( (key, t) );
            }
            return total;
        }

        public static bool AddNotes( Track track, List<Note> notes ) {
            notes.ForEach( x => {
                x.Track = track;
                Repository<Note>.Add( x );
            } );
            return true;
        }

        private const string format = "{0,-10} | {1,-40} | {2,-60} | {3,25} | {4,30} | {5,20} | {6,20}";
        private const string formatPartial = "{0,-10} | {1,-40} |{2,-39}|{3,10}|{4,10} | {5,25} | {6,30} | {7,20} | {8,20}";

        public static void Print( Track track, bool head = true, State? state = null ) {
            Console.ForegroundColor = ConsoleColor.White;
            track.Project = Repository<Project>.GetById( track.Project.Id );
            track.Collaborator = Repository<People>.GetById( track.Collaborator.Id );
            track.Intervals = Repository<Interval>.Get( x => x.Track.Id == track.Id ).ToList( );
            if ( head ) {
                Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine( format, "ID", "PROJECT", "ACTIVITY", "STARTED IN", "FINISHED ON", "TOTAL TIME", "LAST STATE" );
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
            }
            if ( track.Intervals.Any( ) ) {
                var date = track.Intervals.Last( );
                if ( date.End.HasValue )
                    Console.WriteLine( format, track.Id, ( track.Project.Name.Length > 40 ? new String( track.Project.Name.Take( 37 ).ToArray( ) ) + "..." : track.Project.Name ), ( track.Activity.Length > 60 ? new String( track.Activity.Take( 57 ).ToArray( ) ) + "..." : track.Activity ), date.Start.ToString( "dd/MM/yyyy - HH:mm:ss" ), date.End.Value.ToString( "dd/MM/yyyy - HH:mm:ss" ), ( date.End - date.Start ).Value.ToString( @"hh\:mm\:ss" ), date.State );
                else
                    Console.WriteLine( format, track.Id, ( track.Project.Name.Length > 40 ? new String( track.Project.Name.Take( 37 ).ToArray( ) ) + "..." : track.Project.Name ), ( track.Activity.Length > 60 ? new String( track.Activity.Take( 57 ).ToArray( ) ) + "..." : track.Activity ), date.Start.ToString( "dd/MM/yyyy - HH:mm:ss" ), "", "", date.State );
            } else
                Console.WriteLine( format, track.Id, ( track.Project.Name.Length > 40 ? new String( track.Project.Name.Take( 37 ).ToArray( ) ) + "..." : track.Project.Name ), ( track.Activity.Length > 60 ? new String( track.Activity.Take( 57 ).ToArray( ) ) + "..." : track.Activity ), "", "", "", "TO DO" );
            Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
        }

        public static void Print( List<Track> tracks, bool head = true, bool intervals = false ) {
            Console.ForegroundColor = ConsoleColor.White;
            tracks.ToList( ).ForEach( x => {
                x.Project = Repository<Project>.GetById( x.Project.Id );
                x.Collaborator = Repository<People>.GetById( x.Collaborator.Id );
                x.Intervals = Repository<Interval>.Get( z => z.Track.Id == x.Id ).ToList( );
            } );
            if ( head ) {
                Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine( format, "ID", "PROJECT", "ACTIVITY", "STARTED IN", "FINISHED ON", "TOTAL TIME", "LAST STATE" );
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                if ( intervals ) {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine( formatPartial, "", "", "", "INTERVAL", "EXTRA", "RESUMED ON", "PAUSED ON", "PARTIAL TIME", "STATE" );
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
                }
            }
            foreach ( var track in tracks ) {
                TimeSpan? total = new TimeSpan( );
                var i = 1;
                Console.ForegroundColor = ConsoleColor.DarkGray;
                if( track.Intervals.Any())
                foreach ( var date in track.Intervals ) {
                    if ( date.End.HasValue ) {
                        if ( intervals )
                            Console.WriteLine( formatPartial, "", "", "", $"{i}", date.ExtraHour ? "YES" : "NO", date.Start.ToString( "dd/MM/yyyy - HH:mm:ss" ), date.End.Value.ToString( "dd/MM/yyyy - HH:mm:ss" ), ( date.End - date.Start ).Value.ToString( @"hh\:mm\:ss" ), date.State );
                        total += ( date.End - date.Start );
                    } else if ( intervals ) {
                        Console.WriteLine( formatPartial, "", "", "", $"{i}", date.ExtraHour ? "YES" : "NO", date.Start.ToString( "dd/MM/yyyy - HH:mm:ss" ), "", ( DateTime.Now - date.Start ).ToString( @"hh\:mm\:ss" ), date.State );
                        total += ( DateTime.Now - date.Start );
                    }
                    i++;
                }
                Console.ForegroundColor = ConsoleColor.White;
                if ( track.Intervals.Any( ) ) {
                    var last = track.Intervals.Last( );
                    if ( last.End.HasValue )
                        Console.WriteLine( format, track.Id, ( track.Project.Name.Length > 40 ? new String( track.Project.Name.Take( 37 ).ToArray( ) ) + "..." : track.Project.Name ), ( track.Activity.Length > 60 ? new String( track.Activity.Take( 57 ).ToArray( ) ) + "..." : track.Activity ), track.Intervals.First( ).Start.ToString( "dd/MM/yyyy - HH:mm:ss" ), last.End.Value.ToString( "dd/MM/yyyy - HH:mm:ss" ), total.Value.ToString( @"hh\:mm\:ss" ), last.State );
                    else
                        Console.WriteLine( format, track.Id, ( track.Project.Name.Length > 40 ? new String( track.Project.Name.Take( 37 ).ToArray( ) ) + "..." : track.Project.Name ), ( track.Activity.Length > 60 ? new String( track.Activity.Take( 57 ).ToArray( ) ) + "..." : track.Activity ), track.Intervals.First( ).Start.ToString( "dd/MM/yyyy - HH:mm:ss" ), "", total.Value.ToString( @"hh\:mm\:ss" ), last.State );
                }else
                    Console.WriteLine( format, track.Id, ( track.Project.Name.Length > 40 ? new String( track.Project.Name.Take( 37 ).ToArray( ) ) + "..." : track.Project.Name ), ( track.Activity.Length > 60 ? new String( track.Activity.Take( 57 ).ToArray( ) ) + "..." : track.Activity ), "", "", "", "TO DO" );
                Console.WriteLine( "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" );
            }
        }

        public static IEnumerable<List<T>> SplitList<T>( List<T> locations, int nSize = 30 ) {
            for ( int i = 0; i < locations.Count; i += nSize ) {
                yield return locations.GetRange( i, Math.Min( nSize, locations.Count - i ) );
            }
        }

        public static string TimeSpanToTime( TimeSpan time ) {
            return $"{( int ) time.TotalHours}:{time.Minutes}:{time.Seconds}";
        }

    }
}
